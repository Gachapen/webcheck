FROM liuchong/rustup:nightly-musl as builder

WORKDIR /src
COPY . /src
RUN cargo build --release


FROM alpine:latest
RUN apk add ca-certificates && rm -rf /var/cache/apk/*

COPY --from=builder /src/target/x86_64-unknown-linux-musl/release/webcheck /usr/bin/
RUN chmod 500 /usr/bin/webcheck

WORKDIR /data

ENTRYPOINT [ "/usr/bin/webcheck" ]