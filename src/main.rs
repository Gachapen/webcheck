#![feature(rust_2018_preview, use_extern_macros)]

use env_logger;
use log::{debug, error, info, log, warn};
use regex::Regex;
use reqwest;
use serde_derive::{Deserialize, Serialize};
use serde_yaml;
use std::{
    collections::HashMap,
    error::Error,
    fmt::{self, Display, Formatter},
    fs::File,
    iter::FromIterator,
    path::Path,
    time::{Duration, Instant},
};
use tokio::{self, prelude::*, timer::Interval};

const SUCCESS_FILE_PATH: &str = ".success";

fn main() -> Result<(), Box<Error>> {
    env_logger::init();

    if Path::new(SUCCESS_FILE_PATH).exists() {
        info!("Check has already succeeded! To force a re-check, delete the .success file.");
        return Ok(());
    }

    let cfg_file = match File::open("webcheck.yml") {
        Ok(file) => file,
        Err(error) => {
            error!(
                "A config file 'webcheck.yml' is required. Error: {}.",
                error
            );
            std::process::exit(1);
        }
    };

    let cfg: Config = match serde_yaml::from_reader(cfg_file) {
        Ok(cfg) => cfg,
        Err(error) => {
            error!("Failed to parse config file: {}", error);
            std::process::exit(1);
        }
    };
    let regex = Regex::new(&cfg.match_regex)?;

    let check_status = if let Some(more_than) = cfg.more_than {
        move |text: &str| {
            let num_matches = regex.find_iter(&text).count();
            num_matches > more_than as usize
        }
    } else {
        return Err(Box::new(AppError::InvalidChecker));
    };

    let interval = Interval::new(Instant::now(), Duration::from_secs(u64::from(cfg.interval)));
    let task = interval
        .map_err(|error| {
            error!("Failed to run interval: {}", error);
            std::process::exit(1);
        })
        .for_each(move |_| {
            let mut response = match reqwest::get(&cfg.url) {
                Ok(response) => response,
                Err(error) => {
                    error!("Failed to execute request: {}", error);
                    return Ok(());
                }
            };
            let body = match response.text() {
                Ok(response) => response,
                Err(error) => {
                    error!("Failed to parse request body as text: {}", error);
                    return Ok(());
                }
            };

            if !check_status(&body) {
                debug!("Check failed...");
                return Ok(());
            }

            debug!("Check succeeded!");

            if let Err(error) = push_notification(&cfg.pushover) {
                error!("Failed to send push notification: {}", error);
                return Ok(());
            }

            let mut success_file = match File::create(SUCCESS_FILE_PATH) {
                Ok(file) => file,
                Err(error) => {
                    warn!(
                        "Failed to create .success file for saving success: {}",
                        error
                    );
                    return Ok(());
                }
            };

            if let Err(error) = success_file.write_all(b"") {
                warn!("Failed to write to success file: {}", error);
                return Ok(());
            }

            Ok(())
        });

    tokio::run(task);

    Ok(())
}

fn push_notification(cfg: &PushoverConfig) -> Result<(), reqwest::Error> {
    let client = reqwest::Client::new();
    let body = HashMap::<&str, &str>::from_iter(
        vec![
            ("token", cfg.token.as_str()),
            ("user", cfg.user.as_str()),
            ("message", cfg.message.as_str()),
        ].into_iter(),
    );
    client
        .post("https://api.pushover.net/1/messages.json")
        .json(&body)
        .send()?;

    Ok(())
}

#[derive(Serialize, Deserialize)]
struct Config {
    url: String,
    #[serde(rename = "match")]
    match_regex: String,
    more_than: Option<i32>,
    interval: u32,
    pushover: PushoverConfig,
}

#[derive(Serialize, Deserialize)]
struct PushoverConfig {
    token: String,
    user: String,
    message: String,
}

#[derive(Debug)]
enum AppError {
    InvalidChecker,
}

impl Display for AppError {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match self {
            AppError::InvalidChecker => write!(fmt, "Not a valid match config"),
        }
    }
}

impl Error for AppError {}
